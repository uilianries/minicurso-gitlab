## GITLAB

#### Minicurso SECCOM 2018

![gitlab](assets/img/logo.png)

---

#### Apresentação disponível em

##### https://bit.ly/minicursogitlab

---?image=assets/img/lego-dark-green.png
@title[Sobre]

@div[left-70]
Olá Mundo!
<br>
<br>
**Uilian Ries**
<br>
<br>
Desenvolvedor C++ e Python
<br>
Trabalha na **@jfrog**
<br>
<br>
@uilianries
<br>
@fa[github] @fa[twitter] @fa[linkedin]
@divend

@div[right-30]
![me](assets/img/me.png)
@divend

---?image=assets/img/vaga-khomp.png

---?image=assets/img/lego-dark-red.png

### AGENDA

* O qué o Gitlab
* Aspectos
* O movimento Gitlab
* Git
* Integração Contínua
* Gitlab CI
* Hands-on

---?image=assets/img/lego-dark-green.png

### O QUE É O GITLAB

---?image=assets/img/lego-dark-green.png

"O GitLab é o primeiro aplicativo único para todo o ciclo de vida DevOps. Somente o GitLab permite DevOps simultâneo, desbloqueando as organizações das restrições do conjunto de ferramentas. O GitLab fornece visibilidade inigualável, níveis mais altos de eficiência. Isso torna o ciclo de vida do software 200% mais rápido, melhorando radicalmente a velocidade dos negócios."

---?image=assets/img/lego-dark-green.png

#### O QUE É O GITLAB

![infograph](assets/img/cicd_pipeline_infograph.png)

---?image=assets/img/lego-dark-red.png

#### O QUE É O GITLAB

* +25K usuários
* +100K downloads do projeto por mês
* +1K contribuidores

---?image=assets/img/lego-dark-green.png

### ASPTECTOS DO GITLAB

---?image=assets/img/lego-dark-green.png

#### REPLETO DE RECURSOS

* Open Source
* Licença MIT
* Nova versão a cada mês
* Dirigido à comunidade
* Ecosistema Integrado

---?image=assets/img/lego-dark-green.png

#### SEGURANÇA

* Arquitetura escalável
* Segurança de código avançada
* Branches protegidos
* 2FA

---?image=assets/img/lego-dark-green.png

#### GITLAB FEITO PELA COMUNIDADE

* API robusta
* Integração com LDAP
* Policies
* Flexibilidade no deploy

---?image=assets/img/lego-dark-green.png

#### E MAIS ...

* Web IDE
* Auto DevOps
* Repositório privado sem limites
* Integração Agile

---?image=assets/img/lego-dark-blue.png

### MOVENDO PARA O GITLAB

###### #movingtogitlab

---?image=assets/img/lego-dark-blue.png

![github](assets/img/microsoft-github.png)

---?image=assets/img/lego-dark-blue.png

![github](assets/img/gitlab-background-moving.png)

---?image=assets/img/lego-dark-red.png

## GIT

---?image=assets/img/lego-dark-red.png

#### GIT

* Sistema de controle de versão mais utilizado
* Mantido e criado pela comunidade (Linus Torvalds)
* Arquitetura distribuída
* Armazenamento baseado em snapshot

---?image=assets/img/lego-dark-red.png

![git-commands](assets/img/git-book.jpg)

---?image=assets/img/lego-dark-red.png

#### COMANDOS PRINCIPAIS

* init
* add
* commit
* status
* log

* push
* pull
* clone

---?image=assets/img/lego-dark-green.png

`$ git init .`

Inicaliza um novo repositório

---?image=assets/img/lego-dark-green.png

`$ git add <path>`

Adiciona um arquivo/diretório para *commit*

---?image=assets/img/lego-dark-green.png

`$ git commit`

Versiona os arquivos preparados

---?image=assets/img/lego-dark-green.png

`$ git status`

Verifica o estado do *branch* atual

---?image=assets/img/lego-dark-green.png

`$ git log`

Lista os *commits* realizados

---?image=assets/img/lego-dark-green.png

`$ git push <remote>`

Envia as modificações para um servidor remoto

---?image=assets/img/lego-dark-green.png

`$ git pull <remote>`

Puxa as modificações do servidor remoto com o local

---?image=assets/img/lego-dark-green.png

`$ git clone <url>`

Copia um projeto remoto e inicializa localmente

---?image=assets/img/lego-dark-green.png

![](assets/img/infograph_first.png)

---?image=assets/img/lego-dark-blue.png

## INTEGRAÇÃO CONTÍNUA

---?image=assets/img/lego-dark-blue.png

“Integração Contínua é uma pratica de desenvolvimento de software onde os membros de um time integram seu trabalho frequentemente, geralmente cada pessoa integra pelo menos diariamente – podendo haver múltiplas integrações por dia. Cada integração é verificada por um build automatizado (incluindo testes) para detectar erros de integração o mais rápido possível. Muitos times acham que essa abordagem leva a uma significante redução nos problemas de integração e permite que um time desenvolva software coeso mais rapidamente.” *Martin Fowler*

---?image=assets/img/lego-dark-blue.png

### GITLAB CI

![](assets/img/gitlab-ci.png)

---?image=assets/img/lego-dark-blue.png

![](assets/img/gitlab_ci_flow.png)

---?image=assets/img/lego-dark-blue.png

#### CONFIGURAÇÃO COMO CÓDIGO

* Gitlab utiliza o arquivo *.gitlab-ci.yml* como descrição
* O arquivo é consumido por um *Runner*
* *Runner* é um escravo do servidor Gitlab CI e executa fielmente os passos do arquivo

---?image=assets/img/lego-dark-red.png

![](assets/img/ci-cd-architecture.png)

---?image=assets/img/lego-dark-blue.png

#### EXEMPLO GITLAB CONFIG .gitlab-ci.yml

```yaml
image: python
before_script:
  - pip install -r requirements.txt
test:
  script:
    - pytest .
deploy:
  script:
    - pip install .
```
---?image=assets/img/lego-dark-blue.png

#### O QUE É POSSÍVEL FAZER COM CI?

* Criar pacotes
* Executar testes unitários
* Implantar em produção
* Usar qualquer linguagem e framework
* Rodar em qualquer plataforma
* Desenvolvimento mobile e até embarcado

---?image=assets/img/lego-dark-red.png

![](assets/img/torvalds.png)

Talks is cheap. Show me the code.

---?image=assets/img/lego-dark-red.png

## HANDS-ON

* Criar conta no Gitlab
* Criar novo projeto
* Disponibilizar página web

---?image=assets/img/lego-dark-green.png

#### REGISTRAR NOVA CONTA

#### https://gitlab.com/users/sign_in

---?image=assets/img/lego-dark-green.png

![gitlab](assets/img/gitlab_sign.png)

---?image=assets/img/lego-dark-green.png

#### CRIAR NOVO PROJETO

#### https://gitlab.com/projects/new

---?image=assets/img/lego-dark-green.png

#### CRIAR NOVO PROJETO

* Utilizar nome único para URL do projeto:
  * uilianries.gitlab.io
* Visibidade: público

---?image=assets/img/lego-dark-green.png

#### EDITANDO O PROJETO

* Utilizar Web IDE ou editor para criar hello world

---?image=assets/img/lego-dark-green.png

![404](assets/img/gitlab_404.png)

---?image=assets/img/lego-dark-green.png

#### EDITANDO O PROJETO

* Adicionar .gitlab-ci.yml com template para HTML

---?image=assets/img/lego-dark-blue.png

#### GITLAB PAGES

* Gitlab como host para o seu site
  * Blog
  * Portfólio
  * Fotos
  * Apresentação de slides

---?image=assets/img/lego-dark-blue.png

#### GITLAB PAGES - O QUE É PRECISO

* Um projeto
* Arquivo de configuração .gitlab-ci.yml
* Um **job** chamado *pages*
* Um diretório chamado *public* com o conteúdo

---?image=assets/img/lego-dark-green.png

#### GITLAB PAGES - EXEMPLOS

#### https://gitlab.com/pages

---?image=assets/img/lego-dark-red.png

#### CRIANDO BLOG COM CONTEÚDO EXTERNO

* Utilizar exemplo do HTML5up para blog

---?image=assets/img/lego-dark-red.png

#### HTML5UP

#### https://html5up.net/

---?image=assets/img/lego-dark-green.png

#### SORTEIO KHOMP

#### https://www.random.org/

---?image=assets/img/lego-dark-white.png

#### REFERÊNCIAS

* https://about.gitlab.com
* https://rogerdudler.github.io/git-guide
* https://martinfowler.com/articles/continuousIntegration.html
* https://about.gitlab.com/features/pages/
* https://about.gitlab.com/features/gitlab-ci-cd/
* https://html5up.net/

---?image=assets/img/lego-dark-white.png

### OBRIGADO!

##### Perguntas ?

Você pode me encontrar em:  

**@uilianries** - twitter, github  
cpplang.slack.com - canais #conan ou #bincrafters  
uilianries@gmail.com  
https://gitlab.com
